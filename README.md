# Japan Cams #
Enjoy the diverse landscapes and views of Japan right from your Dashboard in macOS. Source available under an MIT license. Made in DashCode. Ready to use binary available from downloads section. Tested on 10.6.8 and above, should work on systems as early as 10.4.3.

Controls
--------
On the front of the widget...

- press the play button to skip to the next webcam

- press the pause button to hold the current one

On the back of the widget...

- choose how long a webcam will be displayed

- choose how long the timeout should be before giving up (default is 10 seconds)

Building
--------
Editing and deploying can be done from either version of DashCode, or you can edit the files within the widget bundle with your favourite text/image editors.

Version info
------------

Current version: 2.3
