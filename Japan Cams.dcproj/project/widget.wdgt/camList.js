var camList = new Array (

// 01
"Odaiba, Tokyo",
"http://www.aurora.dti.ne.jp/odaiba-papa/images/image.jpg",
"t",

// 02
"Ononuma, Unazuki, Toyama",
"http://www.hrr.mlit.go.jp/bosai/img/752035.jpg",
"f",

// 03
"TSK Matsue, Shimane",
"http://157.107.188.223/record/current.jpg?rand=",
"f",

// 04
"Sapporo Kita-1 Nishi-8",
"http://www.dosanko.co.jp/weather/camera1_s.jpg",
"f",

// 05
"Jikogudani Monkey Cam",
"http://www.jigokudani-yaenkoen.co.jp/livecam/monkey/image.jpg",
"f",

// 06
"Morioka City, Iwate",
"http://118.22.23.185/snapshotJpeg?Resolution=320x240&Quality=Clarity",
"f",

// 07
"FM Yokohama",
"http://www.fmyokohama.co.jp/images/livecam/seaside.jpg",
"f",

// 08
"Barber Shop",
"http://www.salon-net.org/realtime100.jpg",
"f",

// 09
"Daimonji, Kyoto",
"http://www.kcg.ac.jp/daimon/current.jpg",
"f",

// 10
"Okutsukei, Okayama",
"http://ohtsuri.town.kagamino.lg.jp/snapshotJpeg?Resolution=320x240&Quality=Clarity",
"f",

// 11
"Shinkansen, Okayama",
"http://camera.shinkansen.mydns.jp:8001/SnapshotJPEG?Resolution=320x240&Quality=Clarity",
"f",

// 12
"Kitakami River, Iwate",
"http://www2.thr.mlit.go.jp/iwate/bousai/kitakami/HICP3080.jpg",
"f",

// 13
"Fukushima Airport",
"http://www.pref.fukushima.jp/kukouoffice/photo1.jpg",
"f",

// 14
"Tsuribashi, Tochigi",
"http://www.ktr.mlit.go.jp/kinudamu/cctvsetoaikyou/jpeg/turibashi.jpg",
"f",

// 15
"Mount Iwate",
"http://www-cg.cis.iwate-u.ac.jp/live_cam/mtiwate-l.jpg",
"f",

// 16
"Tako, Chiba",
"http://www.taco.mydns.jp:81/snapshotJpeg?Resolution=320x240&Quality=Clarity",
"f",

// 17
"Toyo Gakuen Station",
"http://live.toyocollege.com/senbayashi/image/live/licam.jpg",
"f",

// 18
"Izumi-ku, Sendai",
"http://www2.biglobe.ne.jp/~norimari/lcpics/licam.jpg",
"f",

// 19
"Higashimononobe, Shiga",
"http://webcam.heishin.jp/images/image1.jpg",
"f",

// 20
"Nozawa Spa, Nagano",
"http://www.nozawa.ne.jp/nozawagumi/yurari/yurari.jpg",
"f",

// 21
"Dantaisan, Ibaraki",
"http://www.sanbonmatsu.com/weather/camera.cgi",
"f",

// 22
"Iwakura, Kyôto",
"http://61.115.78.205/record/current.jpg",
"f",

// 23
"Route 7, Akita",
"http://www2.thr.mlit.go.jp/noshiro/douro/romen/r7/cam2.jpg",
"f",

// 24
"Osaka Pharmaceutical",
"http://msc.oups.ac.jp/cam/n2.jpg",
"f",

// 25
"Gunma Ski Resort",
"http://www.norn.co.jp/home/tmp/live.jpg",
"f",

// 26
"Kawamata Dam, Tochigi",
"http://www.ktr.mlit.go.jp/kinudamu/cctvsetoaikyou/jpeg/kawamata.jpg",
"f",

// 27
"Route 112, Yamagata",
"http://www2.thr.mlit.go.jp/Bumon/J76301/livecam/cctv007.jpg",
"f"

);
