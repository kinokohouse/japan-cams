// global stuff

var bumpTimer = null;
var statusTimer = null;
var outTimer = null;
var bumpJiffies = 10000;
var maxTimeout = 7500;
var flashLength = 200;
var bumpJiffiesIndex = null;
var maxTimeoutIndex = null;

var currentCam = "Images/empty.png";
var currentCamName = "";
var currentBigPic = false;

var camListLength = camList.length
var currentCamIndex = 0;

var isLoading = false;
var isPaused = false;
var isRunning = false;
var OSVersion = 0;
var OSBorder = 6;

function load()
{
    dashcode.setupParts();
    OSVersion = getOSVersion();
    if (window.widget)
    {
        if (widget.preferenceForKey(widget.identifier + "-bumpJiffiesIndex") === "undefined" || isNaN(widget.preferenceForKey(widget.identifier + "-bumpJiffiesIndex")))
        {
            bumpJiffiesIndex = 5;
            widget.setPreferenceForKey(bumpJiffiesIndex,widget.identifier + "-bumpJiffiesIndex");
            readJiffies();
        }
        else
        {
            readJiffies();
        }
        if (widget.preferenceForKey(widget.identifier + "-maxTimeoutIndex") === "undefined" || isNaN(widget.preferenceForKey(widget.identifier + "-maxTimeoutIndex")))
        {
            maxTimeoutIndex = 2;
            widget.setPreferenceForKey(maxTimeoutIndex,widget.identifier + "-maxTimeoutIndex");
            readTimeout();
        }
        else
        {
            readTimeout();
        }
        if (widget.preferenceForKey(widget.identifier + "-currentCamIndex") === "undefined" || isNaN(widget.preferenceForKey(widget.identifier + "-currentCamIndex")) || widget.preferenceForKey(widget.identifier + "-currentCamIndex") > camListLength)
        {
            currentCamIndex = 0;
            widget.setPreferenceForKey(currentCamIndex,widget.identifier + "-currentCamIndex");
        }
        else
        {
            currentCamIndex = widget.preferenceForKey(widget.identifier + "-currentCamIndex");
        }
    }
    nextCam();
}

function readJiffies()
{
    bumpJiffiesIndex = widget.preferenceForKey(widget.identifier + "-bumpJiffiesIndex");
    refresh.options.selectedIndex = bumpJiffiesIndex;
    refresh.options[refresh.options.selectedIndex].selected = true;
    bumpJiffies = parseInt(refresh.options[refresh.options.selectedIndex].value,10);
}

function readTimeout()
{
    maxTimeoutIndex = widget.preferenceForKey(widget.identifier + "-maxTimeoutIndex");
    timeout.options.selectedIndex = maxTimeoutIndex;
    timeout.options[timeout.options.selectedIndex].selected = true;
    maxTimeout = parseInt(timeout.options[timeout.options.selectedIndex].value,10);
}

function remove()
{
	if (bumpTimer != null)
	{
		stopCamTimer();
        isRunning = false;
	}
}

function hide()
{
	if (bumpTimer != null)
	{
		stopCamTimer();
	}
}

function show()
{
    if (!isPaused)
    {
        if (bumpTimer == null)
        {
            startCamTimer();
        }
        if (isRunning)
        {
            nextCam();
        }
        else
        {
            isRunning = true;
        }
    }
}

function sync()
{
}

function showBack(event)
{
	if (bumpTimer != null)
	{
        stopCamTimer();
	}
    var front = document.getElementById("front");
    var back = document.getElementById("back");

    if (window.widget) {
        widget.prepareForTransition("ToBack");
    }

    front.style.display = "none";
    back.style.display = "block";

    if (window.widget) {
        setTimeout('widget.performTransition();', 0);
    }
}

function showFront(event)
{
	if (bumpTimer == null)
	{
		startCamTimer();
    }
    var front = document.getElementById("front");
    var back = document.getElementById("back");

    if (window.widget) {
        widget.prepareForTransition("ToFront");
    }

    front.style.display="block";
    back.style.display="none";

    if (window.widget) {
        setTimeout('widget.performTransition();', 0);
    }
}

if (window.widget) {
    widget.onremove = remove;
    widget.onhide = hide;
    widget.onshow = show;
    widget.onsync = sync;
}

function startCamTimer()
{
	if (bumpTimer != null)
	{
		stopCamTimer()
	}
	bumpTimer = setTimeout("nextCam();", bumpJiffies);
}

function stopCamTimer()
{
		clearTimeout(bumpTimer);
		bumpTimer = null;
}

function startStatusFlashTimer()
{
	if (statusTimer != null)
	{
		clearInterval(statusTimer);
		statusTimer = null;
	}
	statusTimer = setInterval("toggleStatusVisibility();", flashLength);
}

function clearStatusFlashTimer()
{
	if (statusTimer != null)
	{
		clearInterval(statusTimer);
		statusTimer = null;
	}
}

function showButtons(event)
{
    document.getElementById('pauseButton').style.opacity = 1;
    document.getElementById('nextButton').style.opacity = 1;
    document.getElementById('infoButton').style.opacity = 1;
}

function hideButtons(event)
{
    document.getElementById('pauseButton').style.opacity = 0;
    document.getElementById('nextButton').style.opacity = 0;
    document.getElementById('infoButton').style.opacity = 0;
}

function hardenPause(event)
{
    document.getElementById('reloadbutpic').src = "Images/pause-100.png";
}

function softenPause(event)
{
    document.getElementById('reloadbutpic').src = "Images/pause-60.png";
}

function hardenNext(event)
{
    document.getElementById('nextbutpic').src = "Images/next-100.png";
}

function softenNext(event)
{
    document.getElementById('nextbutpic').src = "Images/next-60.png";
}

function hardenInfo(event)
{
    document.getElementById('infobutpic').src = "Images/info-100.png";
}

function softenInfo(event)
{
    document.getElementById('infobutpic').src = "Images/info-60.png";
}

function nextCam()
{
    getCamAndBump();
    statusToLoading();
    if (currentCam == "http://157.107.188.223/record/current.jpg?rand=") {
        var r = (Math.floor(Math.random() * 90000)) + 1;
        currentCam = currentCam + r + ".jpg";
    }
    document.webcam.src = currentCam;
    outTimer = setTimeout("imageTimedOut();",maxTimeout);
}

function nextCamButton()
{
    if (!isLoading)
    {
        isPaused = false;
        nextCam();
    }
}

function pauseCam(event)
{
    if (!isLoading)
    {
        if (!isPaused)
        {
            isPaused = true;
            document.status.src = "Images/paused.png";
            document.status.style.opacity = 1;
            stopCamTimer();
        }
        else
        {
            isPaused = false;
            clearStatus();
            startCamTimer();
        }

     }
}

function imgError()
{
    if (outTimer != null)
	{
        clearTimeout(outTimer);
        outTimer = null;
    }
    document.webcam.src = "Images/error.png";
}

function imageTimedOut()
{
    imgError();
}

function getElementAndBump()
{
    var x = camList[currentCamIndex];
    currentCamIndex++;
    if (currentCamIndex == camListLength)
        {
            currentCamIndex = 0;
        }
    return x;
}

function getCamAndBump()
{
    currentCamName = getElementAndBump();
    currentCam = getElementAndBump();
    currentBigPic = getElementAndBump();
    if (window.widget)
    {
        widget.setPreferenceForKey(currentCamIndex,widget.identifier + "-currentCamIndex");
    }

}

function statusToLoading()
{
    isLoading = true;
    document.status.src = "Images/loading.png";
    document.status.style.opacity = 1;
    startStatusFlashTimer();
}

function clearStatus()
{
    isLoading = false;
    isReloading = false;
    clearStatusFlashTimer();
    document.status.style.opacity = 0;
    document.status.src = "Images/empty.png";
}

function toggleStatusVisibility()
{
    if (document.status.style.opacity == 1) 
    {
        document.status.style.opacity = 0;
    } 
    else 
    {
        document.status.style.opacity = 1;
    }
}

function showImage()
{
	if (outTimer != null)
	{
        clearTimeout(outTimer);
        outTimer = null;
    }
    if ((currentBigPic == "t") && (OSVersion > OSBorder)) {
        webcam.style.width = "auto";
    }
    else {
        webcam.style.width = "240px";
    }
    document.getElementById('text').innerText = currentCamName;
    document.getElementById('text').style.opacity = 1;
    clearStatus();
    startCamTimer();
}

function changeRefresh(event)
{
    bumpJiffiesIndex = refresh.options.selectedIndex;
    bumpJiffies = parseInt(refresh.options[refresh.options.selectedIndex].value,10);
    widget.setPreferenceForKey(bumpJiffiesIndex,widget.identifier + "-bumpJiffiesIndex");
}

function changeTimeout(event)
{
    maxTimeoutIndex = timeout.options.selectedIndex;
    maxTimeout = parseInt(timeout.options[timeout.options.selectedIndex].value,10);
    widget.setPreferenceForKey(maxTimeoutIndex,widget.identifier + "-maxTimeoutIndex");
}

function getOSVersion() {
    var rx = new RegExp(/10_(\d{1,2})_\d{1,2}/gi);
    var version = rx.exec(navigator.appVersion);
    return parseInt(version[1]);
}
